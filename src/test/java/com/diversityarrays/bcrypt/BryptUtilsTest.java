package com.diversityarrays.bcrypt;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BryptUtilsTest {

	@Test
	public void test() {

		BryptUtils bryptUtils = new BryptUtils(11);

		String password = "mypassword";
		String hash = bryptUtils.hash(password);
		boolean verifyHash = bryptUtils.verifyHash(password, hash);
		assertEquals(true,verifyHash);
		
	}

}
