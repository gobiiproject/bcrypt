package com.diversityarrays.bcrypt;

import org.springframework.security.crypto.bcrypt.BCrypt;

public class BryptUtils {

	private final int logRounds;

	public BryptUtils(int logRounds) {
		this.logRounds = logRounds;
	}

	public String hash(String password) {
		return BCrypt.hashpw(password, BCrypt.gensalt(logRounds));
	}

	public boolean verifyHash(String password, String hash) {
		return BCrypt.checkpw(password, hash);
	}

	public static void main(String[] args) {
		if(args.length!=1) {
			String mainClass = getProgramName();
			System.err.println("Usage: java -jar "+mainClass+" "+"<plain text>");
			System.exit(1);
		}else {
			String password = args[0];
			String hash = new BryptUtils(11).hash(password);
			System.out.print(hash);
		}
	}

	private static String getProgramName() {
		StackTraceElement[] stack = Thread.currentThread ().getStackTrace ();
		StackTraceElement main = stack[stack.length - 1];
		String mainClass = main.getClassName ();
		return mainClass;
	}

}
